// Fill out your copyright notice in the Description page of Project Settings.

#include "BasePlayer.h"
#include "Engine/World.h"		

//Get Mouse Position
#include "GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
ABasePlayer::ABasePlayer()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Initialize Components
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh"); //string arg is the components internal name used by the engine
	//Set up hierarchy
	RootComponent = Mesh;
	Mesh->SetSimulatePhysics(true);
	Mesh->SetEnableGravity(false);

	this->ShipLocation = CreateDefaultSubobject<USceneComponent>("ShipHolder");
	ShipLocation->SetupAttachment(RootComponent);
	this->GunHolder = CreateDefaultSubobject<USceneComponent>("GunHolder");
	GunHolder->SetupAttachment(RootComponent);
	this->GunPrimaryLocation = CreateDefaultSubobject<USceneComponent>("GunPrimaryLocation");
	GunPrimaryLocation->SetupAttachment(GunHolder);
	this->GunSecondaryLocation = CreateDefaultSubobject<USceneComponent>("GunSecondaryLocation");
	GunSecondaryLocation->SetupAttachment(GunHolder);

	this->MovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>("MovementComponent");
	this->MovementComponent->UpdatedComponent = (RootComponent);
	//SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArmForCamera");
	//Camera = CreateDefaultSubobject<UCameraComponent>("Camera");

	GunHolder->SetRelativeLocation(FVector(0.0f, 0.0f, 30.0f));
	//SpringArm->SetupAttachment(Mesh);
	//Camera->SetupAttachment(SpringArm);
	//SpringArm->TargetArmLength = 1250;
	////Note: FRotator(y,x,z)
	//SpringArm->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));

	MovementForce = 10000;
}

APlayerController* PlayerController;
// Called when the game starts or when spawned
void ABasePlayer::BeginPlay()
{
	Super::BeginPlay();

	UWorld* const World = GetWorld();

	//Spawn Ship
	FVector Location = GetActorLocation();
	FRotator Rotation = GetActorRotation();
	//FActorSpawnParameters SpawnInfo;
	//ShipActor = GetWorld()->SpawnActor<ABaseShip>(Location, Rotation, SpawnInfo);
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = this;

		ABaseShip* SpawnedShip = World->SpawnActor<ABaseShip>(ShipData, Location, Rotation, SpawnParams);
		if (SpawnedShip) {
			UE_LOG(LogTemp, Log, TEXT("SPAWN SHIP is spawn"));
			FAttachmentTransformRules rules = FAttachmentTransformRules::SnapToTargetNotIncludingScale;
			rules.bWeldSimulatedBodies = true;
			SpawnedShip->AttachToComponent(ShipLocation, rules);
			ShipActor = SpawnedShip;
		}


		ABaseGun* SpawnedGun1 = World->SpawnActor<ABaseGun>(GunPrimaryData, Location, Rotation, SpawnParams);
		if (SpawnedGun1) {
			UE_LOG(LogTemp, Log, TEXT("SPAWN GUN 1 is spawn"));
			FAttachmentTransformRules rules = FAttachmentTransformRules::SnapToTargetNotIncludingScale;
			rules.bWeldSimulatedBodies = true;
			SpawnedGun1->AttachToComponent(GunPrimaryLocation, rules);
			GunPrimaryActor = SpawnedGun1;
		}

		ABaseGun* SpawnedGun2 = World->SpawnActor<ABaseGun>(GunSecondaryData, Location, Rotation, SpawnParams);
		if (SpawnedGun2) {
			UE_LOG(LogTemp, Log, TEXT("SPAWN GUN 2 is spawn"));
			FAttachmentTransformRules rules = FAttachmentTransformRules::SnapToTargetNotIncludingScale;
			rules.bWeldSimulatedBodies = true;
			SpawnedGun2->AttachToComponent(GunSecondaryLocation, rules);
			GunSecondaryActor = SpawnedGun2;
		}



		//Set Not moving
		IsMoving = false;
		if (ShipActor != NULL)
			ShipActor->DisableMovementVFX();
	}
	//
	//UE_LOG(LogTemp, Log, TEXT("SPAWN SHIP %s"), ShipActor->GetActorLocation().ToString());


	//Get Player Controller
	PlayerController = Cast<APlayerController>(GetController());
	//Set Mouse Cursor
	if (PlayerController)
	{
		PlayerController->bShowMouseCursor = true;
		PlayerController->bEnableClickEvents = true;
		PlayerController->bEnableMouseOverEvents = true;
	}
}

void ABasePlayer::FaceGunToMousePosition(FVector Position)
{
	FVector ThisPosition = GetActorLocation();
	FVector Direction = ThisPosition - ThisPosition;
	FRotator Rotation = Direction.Rotation();
	GunHolder->SetWorldRotation(Rotation);
	UE_LOG(LogTemp, Log, TEXT("MOUSE POSITIOn: %s %s"), *Position.ToString(), *ThisPosition.ToString());
}



// Called every frame
void ABasePlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (GetLastMovementInputVector().Size() > 0)
	{
		if (IsMoving == false)
		{
			IsMoving = true;
			if (ShipActor != NULL)
				ShipActor->EnableMovementVFX();
			UE_LOG(LogTemp, Log, TEXT("MOVING: %s"), IsMoving ? TEXT("True") : TEXT("False"));
		}
	}
	else
	{
		if (IsMoving == true)
		{
			IsMoving = false;
			if (ShipActor != NULL)
				ShipActor->DisableMovementVFX();
			UE_LOG(LogTemp, Log, TEXT("MOVING: %s"), IsMoving ? TEXT("True") : TEXT("False"));
		}
	}

	//Set Ship Rotation
	if (IsMoving)
		ShipActor->SetActorRotation(MovementDirection.Rotation());


	FVector MousePosition;
	FVector MouseDirection;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->DeprojectMousePositionToWorld(MousePosition, MouseDirection);
	FaceGunToMousePosition(MousePosition);
}

// Called to bind functionality to input
void ABasePlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	InputComponent->BindAxis("MoveForward", this, &ABasePlayer::MoveUp);
	InputComponent->BindAxis("MoveRight", this, &ABasePlayer::MoveRight);
	InputComponent->BindAxis("Fire1", this, &ABasePlayer::FireGunPrimary);
}


void ABasePlayer::MoveUp(float Value)
{
	if (Value != 0)
	{
		FVector Direction = FVector(1, 0, 0);
		FVector ForceToAdd = Direction * MovementForce * Value;
		//Mesh->AddForce(ForceToAdd);
		AddMovementInput(Direction, Value * MovementForce);
		UE_LOG(LogTemp, Log, TEXT("Up: %s %s"), *ForceToAdd.ToString(), *Mesh->GetComponentVelocity().ToString());
	}
	MovementDirection = GetLastMovementInputVector();
}


void ABasePlayer::MoveRight(float Value)
{
	if (Value != 0)
	{
		FVector Direction = FVector(0, 1, 0);
		FVector ForceToAdd = Direction * MovementForce * Value;
		//Mesh->AddForce(ForceToAdd);
		AddMovementInput(Direction, Value * MovementForce);
		UE_LOG(LogTemp, Log, TEXT("Right: %s %s"), *ForceToAdd.ToString(), *Mesh->GetComponentVelocity().ToString());
	}
	MovementDirection = GetLastMovementInputVector();
}

void ABasePlayer::ToggleMovementFX(bool isOn)
{

}


void ABasePlayer::FireGunPrimary(float Value)
{
	if (Value > 0)
		if (GunPrimaryActor)
		{
			UE_LOG(LogTemp, Log, TEXT("Calling Fire1"));
			GunPrimaryActor->Shoot();
		}
}

void ABasePlayer::FireGunSecondary(float Value)
{
	if (Value > 0)
		if (GunSecondaryActor)
		{
			UE_LOG(LogTemp, Log, TEXT("Calling Fire2"));
			GunSecondaryActor->Shoot();
		}
}
