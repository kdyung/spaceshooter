// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseShip.h"
#include "BaseGun.h"
#include "Components/InputComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

#include "BasePlayer.generated.h"

UCLASS()
class SPACESHOOTER_API ABasePlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABasePlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void FaceGunToMousePosition(FVector Position);

	UFloatingPawnMovement* MovementComponent;


private:
	void ToggleMovementFX(bool isOn);
	FVector MovementDirection;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float MovementForce;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Mesh;

	//Data to spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ABaseShip> ShipData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ABaseGun> GunPrimaryData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ABaseGun> GunSecondaryData;

	//Objects for Spawn Location
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USceneComponent* ShipLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USceneComponent* GunHolder;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USceneComponent* GunPrimaryLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USceneComponent* GunSecondaryLocation;

	//Spawned on Play
	UPROPERTY(BlueprintReadOnly)
		ABaseShip* ShipActor;
	UPROPERTY(BlueprintReadOnly)
		ABaseGun* GunPrimaryActor;
	UPROPERTY(BlueprintReadOnly)
		ABaseGun* GunSecondaryActor;


	/*
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* Camera;
	*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsMoving = false;

	void MoveUp(float Value);
	void MoveRight(float Value);

	UFUNCTION(BlueprintCallable)
		void FireGunPrimary(float Value);
	UFUNCTION(BlueprintCallable)
		void FireGunSecondary(float Value);
};
