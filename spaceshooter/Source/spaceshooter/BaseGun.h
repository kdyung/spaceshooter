// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseBullet.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "BaseGun.generated.h"

UCLASS()
class SPACESHOOTER_API ABaseGun : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseGun();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void OnFireDelayEnd();



public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USceneComponent* BulletSpawnLocation;

	UPROPERTY(EditDefaultsOnly)
		AActor* BulletActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABaseBullet> BulletData;


	UFUNCTION(BlueprintCallable)
		void Shoot();

	UPROPERTY(BlueprintReadOnly)
		bool CanShoot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float FireDelay;

};
