// Fill out your copyright notice in the Description page of Project Settings.


#include "TimerManager.h" // For gun fire cooldown
#include "BaseBullet.h"

// Sets default values
ABaseBullet::ABaseBullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>("BulletMesh"); //string arg is the components internal name used by the engine
	BulletMesh->SetupAttachment(Root);
	BulletMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	//ShipMesh->SetEnableGravity(false);

	BulletMesh->SetupAttachment(RootComponent);


	this->ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	this->ProjectileMovement->UpdatedComponent = (RootComponent);
	//RootComponent = BulletMesh;
}

// Called when the game starts or when spawned
void ABaseBullet::BeginPlay()
{
	Super::BeginPlay();
	InitializeBullet();
}


// Called every frame
void ABaseBullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseBullet::BeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//Kill Bullet
}

void ABaseBullet::InitializeBullet()
{
	ProjectileMovement->InitialSpeed = BulletSpeed;
	ProjectileMovement->Activate();


	SetLifeSpan(BulletLifeTime);
}

