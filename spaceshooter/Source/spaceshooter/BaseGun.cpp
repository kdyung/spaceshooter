// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseGun.h"
#include "TimerManager.h" // For gun fire cooldown
#include "Engine/World.h"		

// Sets default values
ABaseGun::ABaseGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Initialize Components
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh"); //string arg is the components internal name used by the engine
	//Set up hierarchy
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetSimulatePhysics(false);

	this->BulletSpawnLocation = CreateDefaultSubobject<USceneComponent>("BulletSpawnLocation");
	BulletSpawnLocation->SetupAttachment(RootComponent);

	CanShoot = true;
}

// Called when the game starts or when spawned
void ABaseGun::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ABaseGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseGun::Shoot()
{
	if (CanShoot)
	{
		FActorSpawnParameters SpawnParams;

		UWorld* const World = GetWorld();

		//Spawn Ship
		FVector Location = BulletSpawnLocation->GetComponentLocation();
		FRotator Rotation = BulletSpawnLocation->GetComponentRotation();

		BulletActor = World->SpawnActor<ABaseBullet>(BulletData, Location, Rotation, SpawnParams);

		if (BulletActor != NULL)
		{
			UE_LOG(LogTemp, Log, TEXT("Gun Shooting"));
			CanShoot = false;

			//Delay shots until fire delay has elapsed
			FTimerHandle UnusedHandle;
			GetWorldTimerManager().SetTimer(
				UnusedHandle, this, &ABaseGun::OnFireDelayEnd, FireDelay, false);
		}
	}
}

void ABaseGun::OnFireDelayEnd()
{
	CanShoot = true;
}
