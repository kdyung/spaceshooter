// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseShip.h"

// Sets default values
ABaseShip::ABaseShip()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	/*Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;*/

	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>("ShipMesh"); //string arg is the components internal name used by the engine
	ShipMesh->SetupAttachment(Root);
	ShipMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	//ShipMesh->SetEnableGravity(false);

	RootComponent = ShipMesh;

	ShipExhaustPS = CreateDefaultSubobject<UParticleSystemComponent>("ShipExhaustParticleSystem");
	ShipExhaustPS->SetupAttachment(ShipMesh);
	ShipExhaustPS->bAutoActivate = false;
	ShipExhaustPS->ActivateSystem(false);
}

// Called when the game starts or when spawned
void ABaseShip::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseShip::EnableMovementVFX() const
{
	ShipExhaustPS->ActivateSystem();
}

void ABaseShip::DisableMovementVFX() const
{
	ShipExhaustPS->DeactivateSystem();
}